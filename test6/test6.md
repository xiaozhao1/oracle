<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。



## 1.表及表空间设计方案：

本方案设计4张表：商品表、客户表、订单表和订单详情表。

其中，商品表记录所有在销售系统中销售的商品信息，包括商品ID、名称、描述、价格等；客户表记录所有在销售系统中注册的客户信息，包括客户ID、姓名、联系方式等；订单表记录所有客户下单的订单信息，包括订单ID、下单客户ID、订单日期等；订单详情表记录每个订单中包含的商品及其购买数量等详细信息。

为了提高性能，本方案设计两个表空间：DATA1用于存储商品表和客户表，DATA2用于存储订单表和订单详情表。两个表空间分别位于不同的硬盘上，以减少磁盘IO竞争。

使用 FOR 循环和 INSERT INTO 语句向商品表、客户表、订单表和订单详情表中插入数据。为了生成模拟数据，我们假设商品表中有 100,000 个商品，客户表中有 10,000 个客户，订单表中有 50,000 个订单，订单详情表中有 100,000 个订单详情。



## 2.表结构如下：

商品表（product）：
- id int PRIMARY KEY
- name varchar2(100) NOT NULL
- description varchar2(1000) DEFAULT NULL
- price number(10,2) NOT NULL

客户表（customer）：
- id int PRIMARY KEY
- name varchar2(100) NOT NULL
- email varchar2(100) DEFAULT NULL
- phone varchar2(20) NOT NULL

订单表（order）：
- id int PRIMARY KEY
- customer_id int NOT NULL
- order_date date NOT NULL

订单详情表（order_detail）：
- id int PRIMARY KEY
- order_id int NOT NULL
- product_id int NOT NULL
- quantity int NOT NULL

## 3.设计权限及用户分配方案：

本方案设计两个用户：SALES_USER和ANALYST_USER。SALES_USER用户拥有商品表、客户表、订单表的增删改查权限；ANALYST_USER用户仅拥有订单表和订单详情表的查询权限。

为了确保数据的安全性，对于SALES_USER用户，通过角色控制对商品表和客户表的访问权限。创建名为SALES_ROLE的角色，并将SALES_USER用户授予该角色。然后，将SALES_ROLE角色授予商品表和客户表的SELECT、INSERT、UPDATE、DELETE权限。对于ANALYST_USER用户，直接将订单表和订单详情表的SELECT权限授予该用户即可。

创建SALES_ROLE角色的脚本如下：

```
CREATE ROLE SALES_ROLE;

GRANT SELECT, INSERT, UPDATE, DELETE ON product TO SALES_ROLE;
GRANT SELECT, INSERT, UPDATE, DELETE ON customer TO SALES_ROLE;
```

创建SALES_USER和ANALYST_USER用户及其授权的脚本如下：

```
CREATE USER SALES_USER IDENTIFIED BY password;
CREATE USER ANALYST_USER IDENTIFIED BY password;

GRANT CONNECT, RESOURCE TO SALES_USER;
GRANT CONNECT, RESOURCE TO ANALYST_USER;

GRANT SALES_ROLE TO SALES_USER;
GRANT SELECT ON "order" TO ANALYST_USER;
GRANT SELECT ON order_detail TO ANALYST_USER;
```

## 4.在数据库中建立一个程序包

在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑：

本方案创建一个名为SALES_PACKAGE的程序包，其中包含两个存储过程和一个函数。其中，一个存储过程用于添加新的商品，一个存储过程用于验证客户是否需要验证。同时，设计了一个函数用于计算指定客户所有订单的总金额。

程序包及程序包中包含的存储过程和函数的脚本如下：

```
CREATE OR REPLACE PACKAGE SALES_PACKAGE IS

    PROCEDURE ADD_PRODUCT (
        p_name IN VARCHAR2,
        p_description IN VARCHAR2,
        p_price IN NUMBER);

    PROCEDURE VERIFY_CUSTOMER (
        p_customer_id IN INT,
        p_email IN VARCHAR2,
        p_phone IN VARCHAR2);

    FUNCTION CALC_ORDER_TOTAL (
        p_customer_id IN INT) RETURN NUMBER;

END SALES_PACKAGE;
/

CREATE OR REPLACE PACKAGE BODY SALES_PACKAGE IS

    PROCEDURE ADD_PRODUCT (
        p_name IN VARCHAR2,
        p_description IN VARCHAR2,
        p_price IN NUMBER) IS
    BEGIN
        INSERT INTO product (id, name, description, price) VALUES (product_seq.NEXTVAL, p_name, p_description, p_price);
        COMMIT;
    END ADD_PRODUCT;

    PROCEDURE VERIFY_CUSTOMER (
        p_customer_id IN INT,
        p_email IN VARCHAR2,
        p_phone IN VARCHAR2) IS
        v_customer_email customer.email%TYPE;
        v_customer_phone customer.phone%TYPE;
    BEGIN
        SELECT email, phone INTO v_customer_email, v_customer_phone FROM customer WHERE id = p_customer_id;
        IF v_customer_email <> p_email OR v_customer_phone <> p_phone THEN
            RAISE_APPLICATION_ERROR(-20001, 'Invalid customer information');
        END IF;
        -- do something else
    END VERIFY_CUSTOMER;

    FUNCTION CALC_ORDER_TOTAL (
        p_customer_id IN INT) RETURN NUMBER IS
        v_total NUMBER;
    BEGIN
        SELECT SUM(price * quantity)
        INTO v_total
        FROM order_detail d
        JOIN product p ON d.product_id = p.id
        JOIN "order" o ON d.order_id = o.id
        WHERE o.customer_id = p_customer_id;
        RETURN v_total;
    END;

END SALES_PACKAGE;
/
```



## 5.创建序列：

​	创建了四个序列，分别为 `product_seq`、`customer_seq`、`order_seq` 和 `order_detail_seq` 。这些序列是用于生成主键 ID 的，它们在插入数据时会自动递增。

```
CREATE SEQUENCE product_seq
  MINVALUE 1
  MAXVALUE 99999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;
  
CREATE SEQUENCE customer_seq
  MINVALUE 1
  MAXVALUE 99999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;
  
CREATE SEQUENCE order_seq
  MINVALUE 1
  MAXVALUE 99999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;
  
CREATE SEQUENCE order_detail_seq
  MINVALUE 1
  MAXVALUE 99999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;
```



## 6.创建触发器：

​	创建触发器：创建了四个触发器，分别为 `product_id_trigger`、`customer_id_trigger`、`order_id_trigger` 和 `order_detail_id_trigger`。这些触发器是用于在插入数据时生成主键 ID 的。当插入新数据时，触发器会自动调用序列，生成唯一的 ID，并将其插入相应的数据行中。

```
CREATE OR REPLACE TRIGGER product_id_trigger
BEFORE INSERT ON product
FOR EACH ROW
BEGIN
  SELECT product_seq.NEXTVAL INTO :new.id FROM dual;
END;

CREATE OR REPLACE TRIGGER customer_id_trigger
BEFORE INSERT ON customer
FOR EACH ROW
BEGIN
  SELECT customer_seq.NEXTVAL INTO :new.id FROM dual;
END;

CREATE OR REPLACE TRIGGER order_id_trigger
BEFORE INSERT ON "order"
FOR EACH ROW
BEGIN
  SELECT order_seq.NEXTVAL INTO :new.id FROM dual;
END;

CREATE OR REPLACE TRIGGER order_detail_id_trigger
BEFORE INSERT ON order_detail
FOR EACH ROW
BEGIN
  SELECT order_detail_seq.NEXTVAL INTO :new.id FROM dual;
END;
```



## 7.创建视图：

​	创建视图：创建了一个名为 `order_details_view` 的视图。这个视图通过连接 `order`、`customer`、`order_detail` 和 `product` 四个表，将它们的某些列合并到一起，并计算了订单总价。它可以用于方便地查询订单信息。

```
CREATE VIEW order_details_view AS
SELECT "order".id AS order_id, customer.name AS customer_name, product.name AS product_name, order_detail.quantity, product.price, (order_detail.quantity * product.price) AS total_price
FROM "order"
JOIN customer ON "order".customer_id = customer.id
JOIN order_detail ON "order".id = order_detail.order_id
JOIN product ON order_detail.product_id = product.id;
```


## 8.备份方案：导入导出

### 8.1采取表方式（T方式），将指定表的数据导出

​    备份某个用户模式下指定的对象（表）。业务数据库通常采用这种备份方式，若备份到本地文件，使用如下命令：

  exp study/123@pdborcl rows=y indexes=n compress=n buffer=50000000 file=080813.dmp log=080813.log tables= yxgl_a，yxgl_b，yxgl_c

### 8.2用户方式（U方式），将指定用户的所有对象及数据导出

   备份某个用户模式下的所有对象。业务数据库通常采用这种备份方式，若备份到本地文件，使用如下命令：

exp study/123@pdborcl owner=jnth rows=y indexes=n compress=n buffer=50000000 file=080813.dmp log=080813.log



### 8.3 全库方式（Full方式），将数据库中的所有对象导出

  备份完整的数据库，备份命令为：

exp study/123@pdborcl rows=y indexes=n compress=n buffer=50000000 full=y file=080813.dmp log=080813.log

数据导入（Import）的过程是数据导出（Export）的逆过程，分别将数据文件导入数据库和将数据库数据导出到数据文件。



### 8.4 恢复备份数据中的指定表

  若从本地文件恢复，使用如下命令：

  imp jnth/thpassword@oracle fromuser=jnth touser=jnth rows=y indexes=n commit=y buffer=50000000 ignore=n file=080813.dmp log=080813.log tables=yxgl_a，yxgl_b，yxgl_c



### 8.5 按照用户模式备份的数据进行恢复

  A. 恢复备份数据的全部内容

  若从本地文件恢复，使用如下命令：

  Imp study/123@pdborcl fromuser=jnth touser=jnth rows=y indexes=n commit=y buffer=50000000 ignore=n file=080813.dmp log=080813.log

  B. 恢复备份数据中的指定表

  若从本地文件恢复，使用如下命令：

  imp study/123@pdborcl fromuser=jnth touser=jnth rows=y indexes=n commit=y buffer=50000000 ignore=n file=080813.dmp log=080813.log tables=yxgl_a，yxgl_b，yxgl_c



### 8.6如果备份方式为完全模式，采用下列恢复方法

  若从本地文件恢复，使用如下命令：

  imp study/123@pdborcl rows=y indexes=n commit=y buffer=50000000 ignore=y full=y file=080813.dmp log=080813.log

  导出、导入的优点：

  ●简单易行；

  ●可靠性高；

  ●不影响数据库的正常运行。
