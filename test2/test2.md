# 实验2：用户及权限管理
# 学号：202010414127       姓名：赵雨琪

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验参考步骤

对于以下的对象名称con_res_role，sale，在实验的时候应该修改为自己的名称。

- 第1步：以system登录到pdborcl，创建角色zyq_role和用户cc，并授权和分配空间：

```sql
$ sqlplus system/123@pdborcl
-- 创建角色wyl并包含resource，connect角色和拥有创建视图的功能
create role wyl;
grant connect, resource, create view to wyl;

-- 创建用户www，并分配表空间
create user www identified by 123;
create tablespace SALETB datafile '/tmp/sale.dbf' size 50m;
alter user www default tablespace SALETB;
grant zyq_role to cc;
--收回角色
revoke zyq_role from cc;
```



- 第2步：新用户cc连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
-- 赋予sale create session
grant create session to www;
$ sqlplus www/123@pdborcl
SQL> show user;
USER 为 "SALE"
SQL>select * from session_privs;
PRIVILEGE
----------------------------------------
CREATE SESSION

SQL>select * from session_roles;
未选定行

<!-- system/123@pdborcl -->
-- 解决cc权限不足
SQL> grant all privileges to www;
SQL>create table customers (id number,name varchar(50)) tablespace SALETB;

SQL> insert into customers(id,name)values(1,'zhao');
insert into customers(id,name)values(2,'wang');

SQL> create view customers_view as select name from customers;

SQL> grant select on customers_view to hr;

SQL> select * from customers_view;
NAME
--------------------------------------------------
zhao
wang


```

- 第3步：用户hr连接到pdborcl，查询cc授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL> select * from cc.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> select * from cc.customers_view;
NAME
--------------------------------------------------
yu
wang
```

> 测试一下用户hr,cc之间的表的共享，只读共享和读写共享都测试一下。
> cc用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL> alter profile default limit failed_login_attempts 3;    
配置文件已更改
```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user cc account unlock;
用户已更改。
```

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色zyq_role和用户cc。
> 新用户cc使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

![2.5](D:\QQdownload\2.5.png)

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。

## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
SQL> drop role zyq_role;
角色已删除。

SQL> drop user cc cascade;
用户已删除。
```

- 
